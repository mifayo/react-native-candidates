import React from 'react';

import MainStack from './src/navigation/mainstack';


export default function App() {
	return (
		<MainStack />
	);
}