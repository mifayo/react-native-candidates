import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Badge = () => {
	return (
		<View style={styles.badgeBox}>
			<Text style={styles.badgeText}>Pay</Text>
		</View>
	)
}

export default Badge

const styles = StyleSheet.create({
	badgeBox: {
		backgroundColor: '#3daae2',
		width: 50,
		height: 30,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 7
	},
	badgeText: {
		color: 'white',
		fontSize: 16
	}
})
