import React from 'react'
import { StyleSheet, ActivityIndicator, View } from 'react-native'

const Loading = () => {
	return (
		<View style={styles.LoadingContainer}>
			<ActivityIndicator size='large' color="#817f82" />
		</View>
	)
}

export default Loading

const styles = StyleSheet.create({
	LoadingContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	}
})
