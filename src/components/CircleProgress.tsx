import React, { FC } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { AnimatedCircularProgress } from 'react-native-circular-progress'

interface Props {
	fill: number;
	amount: string;
	status: string
}

const CircleProgress: FC<Props> = ({ fill, amount, status }) => {
	return (
		<AnimatedCircularProgress
			lineCap='round'
			arcSweepAngle={220}
			rotation={250}
			size={280}
			width={13}
			fill={fill}
			tintColor="#3082b0"
			backgroundColor="#222b32">
			{
				(fill) => (
					// inside Progress Circle
					<View style={styles.insideProgressContainer}>
						<Text style={styles.insideProgressTitle}>Remaining Amount</Text>
						<Text style={styles.insideProgressAmount}>{amount}</Text>
						<View style={styles.bar} />
						{status == 'C' ?
							<Text style={styles.StatusClosed}>Closed</Text>
							:
							<Text style={styles.StatusActive}>Active</Text>
						}
					</View>
				)
			}
		</AnimatedCircularProgress>
	)
}

export default CircleProgress

const styles = StyleSheet.create({
	insideProgressContainer: {
		alignItems: 'center'
	},
	insideProgressTitle: {
		fontSize: 14,
		fontWeight: 'bold',
		color: "#817f82"
	},
	bar: {
		backgroundColor: '#6caab5',
		height: 3,
		width: 50,
		borderRadius: 3,
		marginVertical: 7,
	},
	insideProgressAmount: {
		fontSize: 26,
		fontWeight: 'bold',
		color: "#545154"
	},
	StatusActive: {
		fontSize: 18,
		fontWeight: 'bold',
		color: "#ec4548"
	},
	StatusClosed: {
		fontSize: 18,
		fontWeight: 'bold',
		color: "#6caab5"
	},
})
