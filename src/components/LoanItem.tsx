import React, { FC } from 'react'
import { ScrollView, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Badge, CircleProgress, LoanDetailsItem } from './index';

interface Props {
	navigation: any;
	account_no: string;
	amount_due: string;
	total_loan_amount: string;
	installment_amount: string;
	tenure: string,
	status: string;
	present_balance: string;
}

const LoanItem: FC<Props> = ({
	navigation,
	account_no,
	amount_due,
	total_loan_amount,
	installment_amount,
	tenure,
	status,
	present_balance
}) => {

	// Sum percentage of payements
	const fill_progress: any = 100 * +(+total_loan_amount - +present_balance) / +total_loan_amount;

	//Sum Maturity
	const maturity: number = +present_balance / + installment_amount;

	return (
		<ScrollView style={styles.container}>

			{/* Start Loan details */}
			<View style={styles.LoanWrapper}>
				<LoanDetailsItem title={`Loan Amount`} value={`${total_loan_amount}`} />
				<LoanDetailsItem title={`EMI`} value={`${installment_amount}`} />
				<LoanDetailsItem title={`Tenure`} value={`${tenure}`} />
			</View>
			{/* End Loan details */}

			{/* Start Circle Progress */}
			<TouchableOpacity
				style={styles.ProgressWrapper}
				activeOpacity={0.8}
				onPress={() => navigation.navigate('Payements', { account_no })}
			>
				<CircleProgress
					fill={fill_progress}
					amount={`${parseFloat(present_balance).toFixed(3)}`}
					status={status}
				/>
				<Text style={styles.titleStyle}>Staff Cash Loan</Text>
			</TouchableOpacity>
			{/* End Circle Progress */}

			{/* Start Maturity */}
			<View style={styles.sectionMaturityWrapper}>
				<Text style={styles.titleStyle}>No of months till maturity</Text>
				<Text style={styles.valueStyle}>{Math.round(maturity)}</Text>
			</View>
			{/* End Maturity */}

			{/* Start Due Amount */}
			<View style={styles.sectionDueAmountWrapper}>
				<View style={{ flex: 1 }}>
					<Text style={styles.titleStyle}>Due Amount</Text>
					<Text style={styles.valueStyle}>{amount_due}</Text>
				</View>
				<Badge />
			</View>
			{/* End Due Amount */}

		</ScrollView>
	)
}

export default LoanItem

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white'
	},
	LoanWrapper: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		borderBottomColor: '#E5E5E5',
		borderBottomWidth: .5
	},
	ProgressWrapper: {
		borderBottomColor: '#E5E5E5',
		borderBottomWidth: .5,
		alignItems: 'center',
		paddingVertical: 30,
	},
	sectionMaturityWrapper: {
		borderBottomColor: '#E5E5E5',
		borderBottomWidth: .5,
		paddingVertical: 30,
		paddingHorizontal: 40
	},
	sectionDueAmountWrapper: {
		paddingVertical: 30,
		paddingHorizontal: 40,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	titleStyle: {
		fontSize: 20,
		fontWeight: 'bold',
		color: "#817f82"
	},
	valueStyle: {
		fontSize: 18,
		fontWeight: 'bold',
		color: "#a9b2b8"
	},
})
