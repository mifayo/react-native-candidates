import React, { FC } from 'react'
import { StyleSheet, Text, View } from 'react-native'

interface Props {
	value: string;
	title: string;
}

const LoanDetailsItems: FC<Props> = ({ value, title }) => {
	return (
		<View style={styles.wrapper}>
			<Text style={styles.valueStyle}>{value}</Text>
			<View style={styles.barStyle} />
			<Text style={styles.titleStyle}>{title}</Text>
		</View>
	)
}

export default LoanDetailsItems

const styles = StyleSheet.create({
	wrapper: {
		flex: 1,
		alignItems: 'center',
		paddingVertical: 30,
		borderRightColor: "#E5E5E5",
		borderRightWidth: .5
	},
	valueStyle: {
		fontSize: 17,
		fontWeight: 'bold',
		color: "#5d5b5e"
	},
	barStyle: {
		backgroundColor: '#6caab5',
		height: 3,
		width: 50,
		borderRadius: 3,
		marginVertical: 7,
	},
	titleStyle: {
		fontSize: 14,
		fontWeight: 'bold',
		color: '#a9b0b3'
	}
})
