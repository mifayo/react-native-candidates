import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export const Dot = () => {
	return (
		<View style={styles.dot} />
	)
}

export const ActiveDot = () => {
	return (
		<View style={styles.activeDot} />
	)
}


const styles = StyleSheet.create({
	dot: {
		backgroundColor: '#222b2f',
		width: 11,
		height: 11,
		borderRadius: 8,
		marginLeft: 3,
		marginRight: 3,
		marginTop: 3,
		marginBottom: 3,
	},
	activeDot: {
		backgroundColor: '#3eabdf',
		width: 12,
		height: 12,
		borderRadius: 8.5,
		marginLeft: 3,
		marginRight: 3,
		marginTop: 3,
		marginBottom: 3,
	}
})
