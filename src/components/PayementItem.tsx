import React, { FC } from 'react'
import { StyleSheet, Text, View } from 'react-native'

interface Props {
	amount: string;
	received: string;
	date: string;
	installment: string;
}

const PayementItem: FC<Props> = ({ date, amount, received, installment }) => {
	return (
		<View style={styles.paymentBox}>
			<Text style={styles.paymentText}>Due Date</Text>
			<Text style={styles.paymentDate}>{date}</Text>
			<Text style={styles.paymentText}>Amount: {amount} KWD</Text>
			<Text style={styles.paymentText}>Received Amount: {received} KWD</Text>
			<Text style={styles.paymentText}>Installment Number: {installment}</Text>
		</View>
	)
}

export default PayementItem

const styles = StyleSheet.create({
	paymentBox: {
		borderBottomColor: '#E5E5E5',
		borderBottomWidth: .5,
		paddingVertical: 10,
		paddingHorizontal: 20
	},
	paymentText: {
		color: '#b6bfc5',
		fontSize: 16,
		fontWeight: 'bold'
	},
	paymentDate: {
		color: '#77cf98',
		fontSize: 18,
		fontWeight: 'bold'
	}
})
