import React, { FC } from 'react'
import { NavigationContainer } from '@react-navigation/native'

import AppStack from './appstack'

const MainStack: FC = () => {
	return (
		<NavigationContainer>
			<AppStack />
		</NavigationContainer>
	)
}

export default MainStack;