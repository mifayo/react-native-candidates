import React, { FC } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Account, Payements } from '../screens';
import { StyleSheet, Text } from 'react-native';

const { Navigator, Screen } = createStackNavigator();

const AppStack: FC = () => {
	return (
		<Navigator>
			<Screen name="Account" component={Account} options={{
				headerTitle: () => <Text style={styles.titleStyle}>15231698131</Text>
			}} />
			<Screen name="Payements" component={Payements} options={{
				headerTitle: () => <Text style={styles.titleStyle}>Account Repayements</Text>,
			}} />
		</Navigator>
	)
}

export default AppStack;

const styles = StyleSheet.create({
	titleStyle: {
		fontSize: 20,
		fontWeight: 'bold',
		textAlign: 'center'
	},
	rightTextStyle: {
		fontSize: 16,
		fontWeight: '700',
		textAlign: 'center',
		color: "#2f95dc",
		marginRight: 5
	}
})