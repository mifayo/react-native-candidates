import React, { FC, useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import axios from 'axios';
import { Loading, PayementItem } from '../components'
import { FlatList } from 'react-native-gesture-handler';

interface Repayement {
	id: string;
	due_date: string;
	installment_amount: string;
	received_amount: string;
	number: string
}

interface Params {
	account_no: string;
}

interface Route {
	params: Params;
}

interface Props {
	route: Route;
}

const payements: FC<Props> = ({ route }) => {

	const [loading, setLoading] = useState<boolean | true>(true);
	const [repayements, setRepayements] = useState<Array<Repayement> | []>([]);

	useEffect(() => {
		fetchRepayements(route.params.account_no);
	}, []);

	const fetchRepayements = async (account_no: string) => {
		try {
			// fetch data from api
			const response = await axios.get(`https://api.cfc-kw.com/customers/584657/loans/${account_no}/history`);
			const { data } = response.data;
			// Set data into loans and loading into false
			setRepayements(data.repayment)
			setLoading(false)
		} catch (error) {
			throw error;
		}
	}

	//	If loading still true return Loading Component
	if (loading) {
		return (
			<Loading />
		)
	}

	return (
		<View style={styles.container}>
			{/* List of repayements */}
			<FlatList
				data={repayements}
				keyExtractor={item => item.id}
				renderItem={({ item }) => {
					return (<PayementItem
						amount={`${item.installment_amount}`}
						received={`${item.received_amount}`}
						date={`${item.due_date.split(' ')[0]}`}
						installment={`${item.number}`}
					/>)
				}}
			/>
		</View>
	)
}

export default payements

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white"
	},

})
