import React, { FC, useEffect, useState } from 'react'
import { View } from 'react-native'
import axios from 'axios'
import Swiper from 'react-native-swiper'

import { ActiveDot, Dot, Loading, LoanItem } from '../components/index'


interface Loan {
	account_no: string;
	amount_due: string;
	total_loan_amount: string;
	installment_amount: string;
	tenure: string;
	status: string;
	present_balance: string;
}

interface Props {
	navigation: any
}

const account: FC<Props> = ({ navigation }) => {

	const [loading, setLoading] = useState<boolean | true>(true);
	const [loans, setLoans] = useState<Array<Loan> | []>([]);

	useEffect(() => {
		//fetch list Loan 
		fetchLoan();
	}, [])

	const fetchLoan = async () => {
		try {
			// fetch data from api
			const response = await axios.get('https://api.cfc-kw.com/customers/584657/loans');
			const { data } = response.data;
			// Set data into loans and loading into false
			setLoans(data)
			setLoading(false)
		} catch (error) {

			throw error;
		}
	}

	//	If loading still true return Loading Componenet
	if (loading) {
		return (
			<Loading />
		)
	}

	// If data loaded display it
	return (
		// Vertical Swiper
		<Swiper
			loop={false}
			showsPagination={true}
			dot={<Dot />}
			activeDot={<ActiveDot />}
		>
			{/* Map on Loans List */}
			{loans.map(item => <LoanItem
				key={item.account_no}
				account_no={item.account_no}
				navigation={navigation}
				amount_due={item.amount_due}
				total_loan_amount={item.total_loan_amount}
				installment_amount={item.installment_amount}
				tenure={item.tenure}
				status={item.status}
				present_balance={item.present_balance}
			/>)}
		</Swiper>
	)
}

export default account
